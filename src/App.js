import React, {Component} from 'react';
import './App.css';
import Modal from './components/UI/Modal/Modal';
import Alert from './components/UI/Alert/Alert';
import Button from './components/UI/Button/Button';

class App extends Component {
    state = {
        showModal: false
    };

    showModal = () => {
        this.setState({showModal: true});
    };

    closeModal = () => {
        this.setState({showModal: false});
    };

    closeAlert = (event) => {
        event.target.parentNode.className += ' closed';
    };

    continued = () => {
        alert('You clicked continue!');
    };

    render() {
        const array = [
            {type: 'primary', label: 'Continue', clicked: this.continued},
            {type: 'danger', label: 'Close', clicked: this.closeModal}
        ];

        let button = array.map((obj, i) => <Button key={i} btnType={obj.type} clicked={obj.clicked}>{obj.label}</Button>);

        return (
            <div className="App">
                <button className="button" onClick={this.showModal}><span>Show modal</span></button>
                <Modal
                    show={this.state.showModal}
                    closed={this.closeModal}
                    title="Some kinda modal title"
                >
                    <p>This is modal content</p>
                    {button}
                </Modal>
                <Alert type="primary" dismiss={this.closeAlert}>This is a primary type alert</Alert>
                <Alert type="success">This is a success type alert</Alert>
                <Alert type="warning">This is a warning type alert</Alert>
                <Alert type="danger" dismiss={this.closeAlert}>This is a danger type alert</Alert>
            </div>
        );
    }
}

export default App;
