import React from 'react';
import './Alert.css';

const Alert = props => {
    return (
        <div className={`alert-box ${props.type}`}>{props.children} {props.dismiss ? <span onClick={props.dismiss} className="closeAlert">x</span> : null}</div>
    )
};

export default Alert;